FROM ruby:3.1-alpine 

RUN apk update && apk add build-base nodejs imagemagick postgresql-dev postgresql-client

WORKDIR /app

COPY Gemfile /app/Gemfile

COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler && bundle install



# Add a script to be executed every time the container starts.

# COPY entrypoint.sh /usr/bin/

# RUN chmod +x /usr/bin/entrypoint.sh

# ENTRYPOINT ["entrypoint.sh"]

# EXPOSE 3000

COPY . .

# Configure the main process to run when running the image

# CMD ["rails", "server", "-b", "0.0.0.0"]